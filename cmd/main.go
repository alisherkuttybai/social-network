package main

import (
	"context"
	"log"
	"os"
	"strings"

	"social-network/internal/app"
	"social-network/internal/config"
)

const (
	configPath = "./config/config.yaml"
)

func main() {
	cfgPath := os.Getenv("CONFIG_PATH")
	if strings.TrimSpace(cfgPath) == "" {
		cfgPath = configPath
	}
	if _, err := os.Stat(cfgPath); os.IsNotExist(err) {
		log.Fatalf("config file does not exist: %s", cfgPath)
	}
	cfg, err := config.New(configPath)
	if err != nil {
		log.Fatalf("failed init config: %s", err)
	}
	ctx := context.Background()
	application, err := app.New(ctx, cfg)
	if err != nil {
		log.Fatalf("failed init application: %s", err)
	}
	application.Run()
}
