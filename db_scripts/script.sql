CREATE TABLE IF NOT EXISTS users (
   id SERIAL PRIMARY KEY,
   name VARCHAR (255) NOT NULL,
   surname VARCHAR (255) NOT NULL,
   email VARCHAR (50) UNIQUE NOT NULL,
   password VARCHAR (255) NOT NULL,
   is_admin BOOLEAN DEFAULT 'FALSE',
   created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE EXTENSION IF NOT EXISTS pgcrypto;

insert into users(name, surname, email, password, is_admin) values('tester', 'testovichev', 'test@gmail.com', crypt('test1234', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('admin', 'admin', 'admin@gmail.com', crypt('admin', gen_salt('bf')), true);
insert into users(name, surname, email, password, is_admin) values('bernar', 'bernarov', 'bernar@gmail.com', crypt('bernar00', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('alibi', 'auezov', 'alibi@gmail.com', crypt('auezov', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('develop', 'backend', 'developer@gmail.com', crypt('developer123', gen_salt('bf')), false);

CREATE TABLE IF NOT EXISTS posts (
    id SERIAL PRIMARY KEY,
    title VARCHAR (255) UNIQUE NOT NULL,
    author VARCHAR (255) NOT NULL,
    text VARCHAR (1000) NOT NULL,
    likes NUMERIC NOT NULL,
    is_public BOOLEAN DEFAULT 'FALSE',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

insert into posts(title, author, text, likes, is_public) values('golang', 'developer', 'Go is a statically typed, compiled high-level programming language designed at Google by Robert Griesemer, Rob Pike, and Ken Thompson.', 0, true);
insert into posts(title, author, text, likes, is_public) values('testing', 'test', 'Test post', 1, true);
insert into posts(title, author, text, likes, is_public) values('book review', 'samantha', 'post about book review', 0, true);
insert into posts(title, author, text, likes, is_public) values('grpc', 'alibi', 'gRPC is a cross-platform open source high performance remote procedure call framework.', 2, false);
insert into posts(title, author, text, likes, is_public) values('steam', 'bernar', 'Steam is a video game digital distribution service and storefront developed by Valve Corporation.', 9, false);
