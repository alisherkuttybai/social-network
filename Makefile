start: install-deps vendor-proto gen-proto \
 compose \
 tidy run

run:
	go run ./cmd

tidy:
	go mod tidy

u-test:
	go test -v ./internal/grpc...

i-test:
	go test -v ./internal/app...

postgres:
	docker run --rm -d --name mock-postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgremock -p 5434:5432 postgres:15-alpine

createdb:
	docker exec -it mock-postgres createdb --username=postgres --owner=postgres mock

migrateup:
	migrate -path migrations/mock -database "postgresql://postgres:postgremock@localhost:5434/mock?sslmode=disable" -verbose up

migratedown:
	migrate -path migrations/mock -database "postgresql://postgres:postgremock@localhost:5434/mock?sslmode=disable" -verbose down

build:
	go build -o ./build/main.exe ./cmd/main.go

compose:
	docker compose up -d

install-deps:
	$ go install \
        github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
        google.golang.org/protobuf/cmd/protoc-gen-go \
        google.golang.org/grpc/cmd/protoc-gen-go-grpc

vendor-proto:
	@if [ ! -d vendor.protogen/google ]; then \
		git clone https://github.com/googleapis/googleapis vendor.protogen/googleapis &&\
		mkdir -p  vendor.protogen/google/ &&\
		mv vendor.protogen/googleapis/google/api vendor.protogen/google &&\
		rm -rf vendor.protogen/googleapis ;\
	fi

vendor-swagger:
	@if [ ! -d vendor.protogen/swagger ]; then \
		git clone https://github.com/grpc-ecosystem/grpc-gateway vendor.protogen/grpc-gateway &&\
		mkdir -p  vendor.protogen/swagger/ &&\
		mv vendor.protogen/grpc-gateway/protoc-gen-openapiv2 vendor.protogen/swagger &&\
		rm -rf vendor.protogen/grpc-gateway ;\
	fi

gen-proto:
	mkdir -p protos/pkg/social_network_v1
	protoc --proto_path protos/api/social_network_v1 --proto_path vendor.protogen --proto_path vendor.protogen/swagger  \
	--go_out=protos/pkg/social_network_v1 --go_opt=paths=source_relative \
	--go-grpc_out=protos/pkg/social_network_v1 --go-grpc_opt=paths=source_relative \
	--grpc-gateway_out=protos/pkg/social_network_v1 --grpc-gateway_opt=paths=source_relative \
	--openapiv2_out=protos/api/social_network_v1 \
	protos/api/social_network_v1/social_network.proto
