CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    surname VARCHAR (255) NOT NULL,
    email VARCHAR (50) UNIQUE NOT NULL,
    password VARCHAR (255) NOT NULL,
    is_admin BOOLEAN DEFAULT 'FALSE',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE EXTENSION IF NOT EXISTS pgcrypto;

insert into users(name, surname, email, password, is_admin) values('tester', 'testovichev', 'test@gmail.com', crypt('test1234', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('admin', 'admin', 'admin@gmail.com', crypt('admin', gen_salt('bf')), true);
insert into users(name, surname, email, password, is_admin) values('bernar', 'bernarov', 'bernar@gmail.com', crypt('bernar00', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('alibi', 'auezov', 'alibi@gmail.com', crypt('auezov', gen_salt('bf')), false);
insert into users(name, surname, email, password, is_admin) values('develop', 'backend', 'developer@gmail.com', crypt('developer123', gen_salt('bf')), false);
