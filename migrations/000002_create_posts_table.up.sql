CREATE TABLE IF NOT EXISTS posts (
    id SERIAL PRIMARY KEY,
    title VARCHAR (255) UNIQUE NOT NULL,
    author VARCHAR (255) NOT NULL,
    text VARCHAR (1000) NOT NULL,
    likes NUMERIC NOT NULL,
    is_public BOOLEAN DEFAULT 'FALSE',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

insert into posts(title, author, text, likes, is_public) values('golang', 'developer', 'Go is a statically typed, compiled high-level programming language designed at Google by Robert Griesemer, Rob Pike, and Ken Thompson.', 0, true);
insert into posts(title, author, text, likes, is_public) values('testing', 'test', 'Test post', 1, true);
insert into posts(title, author, text, likes, is_public) values('book review', 'samantha', 'post about book review', 0, true);
insert into posts(title, author, text, likes, is_public) values('grpc', 'alibi', 'gRPC is a cross-platform open source high performance remote procedure call framework.', 2, false);
insert into posts(title, author, text, likes, is_public) values('steam', 'bernar', 'Steam is a video game digital distribution service and storefront developed by Valve Corporation.', 9, false);