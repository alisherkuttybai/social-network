package app

import (
	"context"
	"fmt"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"social-network/internal/config"
	"social-network/internal/grpc/post"
	"social-network/internal/grpc/user"
	"social-network/internal/repository"
	"social-network/internal/service"
	"social-network/pkg/database/postgres"
	"social-network/pkg/logger"
	"social-network/pkg/transport/grpcserver"
	snv1 "social-network/protos/pkg/social_network_v1"
	"syscall"
)

type App struct {
	cfg *config.Config
	log logger.Logger
	db  *postgres.Postgres
	srv *grpcserver.Server
}

func New(ctx context.Context, cfg *config.Config) (*App, error) {
	log, err := logger.New(cfg.App.LogLevel)
	if err != nil {
		return nil, fmt.Errorf("failed init logger: %w", err)
	}
	db, err := postgres.New(&cfg.DB.Postgres)
	if err != nil {
		return nil, fmt.Errorf("failed init db: %w", err)
	}
	srv, err := grpcserver.New(&cfg.Transport)
	if err != nil {
		return nil, fmt.Errorf("failed init server: %w", err)
	}

	repo := repository.New(log, db.Conn)
	s := service.New(log, repo)

	err = initGrpcServices(ctx, log, s, srv)
	if err != nil {
		return nil, fmt.Errorf("failed init server: %w", err)
	}

	return &App{
		cfg: cfg,
		log: log,
		db:  db,
		srv: srv,
	}, nil
}

func (a *App) Run() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	a.log.Debug("debug mode are enabled")
	a.log.Info("application starting",
		slog.String("mode", a.cfg.App.LogLevel),
		slog.String("version", a.cfg.App.Version),
		slog.String("http server", fmt.Sprintf("%s:%s", a.cfg.Transport.HTTP.Host, a.cfg.Transport.HTTP.Port)),
		slog.String("grpc server", fmt.Sprintf("%s:%s", a.cfg.Transport.GRPC.Host, a.cfg.Transport.GRPC.Port)),
		slog.String("swagger server", fmt.Sprintf("%s:%s", a.cfg.Transport.Swagger.Host, a.cfg.Transport.Swagger.Port)),
	)

	a.log.Info("start migrate database from ./migrations")
	if err := a.db.Migrate(); err != nil {
		a.log.Error("failed migrate database", logger.Err(err))
	}
	a.log.Info("end migration")

	a.log.Info("starting servers")

	go func() {
		if err := a.srv.StartGrpc(); err != nil {
			a.log.Error("error grpc server", logger.Err(err))
			log.Fatal(err)
		}
	}()

	go func() {
		if err := a.srv.StartHttp(); err != nil {
			a.log.Error("error http server", logger.Err(err))
			log.Fatal(err)
		}
	}()

	go func() {
		if err := a.srv.StartSwaggerSrv(); err != nil {
			a.log.Error("error swagger server", logger.Err(err))
			log.Fatal(err)
		}
	}()

	a.log.Info("application started")

	sign := <-stop
	a.log.Info("stopping application", slog.String("signal", sign.String()))

	a.Stop()
	a.log.Info("application stopped")
}

func (a *App) Stop() {
	if err := a.db.Stop(); err != nil {
		a.log.Error("failed to stop database", logger.Err(err))
		return
	}
	if err := a.srv.Stop(); err != nil {
		a.log.Error("failed to stop server", logger.Err(err))
		return
	}
}

func initGrpcServices(ctx context.Context, log logger.Logger, s *service.Service, srv *grpcserver.Server) error {
	user.New(log, srv.Grpc, s.User)
	post.New(log, srv.Grpc, s.Post)

	if err := snv1.RegisterUsersHandlerFromEndpoint(ctx, srv.Mux, srv.GrpcAddress, srv.HttpOpts); err != nil {
		return err
	}
	if err := snv1.RegisterPostsHandlerFromEndpoint(ctx, srv.Mux, srv.GrpcAddress, srv.HttpOpts); err != nil {
		return err
	}

	return nil
}
