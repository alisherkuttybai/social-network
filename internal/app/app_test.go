package app

import (
	"context"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"log"
	"social-network/internal/config"
	"social-network/internal/repository"
	"social-network/internal/service"
	"social-network/pkg/database/postgres"
	"social-network/pkg/logger"
	"social-network/pkg/transport/grpcserver"
	snv1 "social-network/protos/pkg/social_network_v1"
	"testing"
)

func setupApp() *App {
	cfg := config.Config{
		App: config.App{
			LogLevel: "dev",
			Name:     "social-network",
			Version:  "0.0.1",
		},
		Transport: config.Transport{
			HTTP: config.Http{
				Host: "localhost",
				Port: "9098",
			},
			GRPC: config.Grpc{
				Host:    "localhost",
				Port:    "55056",
				Timeout: 3,
			},
			Swagger: config.Swagger{
				Host: "localhost",
				Port: "1324",
			},
		},
		DB: config.DB{
			Postgres: config.Postgres{
				Host: "localhost",
				Port: "5434",
				User: "postgres",
				Pass: "postgremock",
				Name: "mock",
			},
		},
	}

	ctx := context.Background()

	log, _ := logger.New(cfg.App.LogLevel)
	db, _ := postgres.New(&cfg.DB.Postgres)
	srv, _ := grpcserver.New(&cfg.Transport)

	repo := repository.New(log, db.Conn)
	s := service.New(log, repo)

	_ = initGrpcServices(ctx, log, s, srv)

	return &App{
		cfg: &cfg,
		log: log,
		db:  db,
		srv: srv,
	}
}

func authInterceptor(authToken string) grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		ctx = metadata.AppendToOutgoingContext(ctx, "authorization", "Bearer "+authToken)
		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

func TestCreateUser(t *testing.T) {
	app := setupApp()

	go func() {
		if err := app.srv.StartGrpc(); err != nil {
			log.Fatalf("failed to serve grpc: %v", err)
		}
	}()

	conn, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	defer func() {
		_ = app.db.Stop()
		_ = app.srv.Stop()
		_ = conn.Close()
	}()

	client := snv1.NewUsersClient(conn)
	ctx := context.Background()
	req := &snv1.CreateUserRequest{
		Info: &snv1.UserInfo{
			Name:     "John",
			Surname:  "Doe",
			Email:    "john.doe@example.com",
			Password: "password123",
			IsAdmin:  false,
		},
	}

	res, err := client.CreateUser(ctx, req)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, "user created", res.GetMessage())
}

func TestUpdateUser(t *testing.T) {
	app := setupApp()

	go func() {
		if err := app.srv.StartGrpc(); err != nil {
			log.Fatalf("failed to serve grpc: %v", err)
		}
	}()

	conn, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	defer func() {
		_ = app.db.Stop()
		_ = app.srv.Stop()
		_ = conn.Close()
	}()

	client := snv1.NewUsersClient(conn)
	ctx := context.Background()

	reqAuth := &snv1.AuthenticateUserRequest{
		Login: &snv1.Login{
			Email:    "admin@gmail.com",
			Password: "admin",
		},
	}
	resAuth, err := client.AuthenticateUser(ctx, reqAuth)
	if err != nil {
		t.Fatalf("failed to authenticate: %v", err)
	}

	authToken := resAuth.GetToken()

	conn2, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(
		insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(authInterceptor(authToken)),
	)
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}
	defer conn2.Close()

	req := &snv1.UpdateUserRequest{
		Id: 6,
		Info: &snv1.UserInfo{
			Email: "johndoe@gmail.com",
		},
	}

	client2 := snv1.NewUsersClient(conn2)
	res, err := client2.UpdateUser(ctx, req)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, "user updated", res.GetMessage())
}

func TestDeleteUser(t *testing.T) {
	app := setupApp()

	go func() {
		if err := app.srv.StartGrpc(); err != nil {
			log.Fatalf("failed to serve grpc: %v", err)
		}
	}()

	conn, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	defer func() {
		_ = app.db.Stop()
		_ = app.srv.Stop()
		_ = conn.Close()
	}()

	client := snv1.NewUsersClient(conn)
	ctx := context.Background()

	reqAuth := &snv1.AuthenticateUserRequest{
		Login: &snv1.Login{
			Email:    "admin@gmail.com",
			Password: "admin",
		},
	}
	resAuth, err := client.AuthenticateUser(ctx, reqAuth)
	if err != nil {
		t.Fatalf("failed to authenticate: %v", err)
	}

	authToken := resAuth.GetToken()

	conn2, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(
		insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(authInterceptor(authToken)),
	)
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}
	defer conn2.Close()

	req := &snv1.DeleteUserRequest{
		Id: 6,
	}

	client2 := snv1.NewUsersClient(conn2)
	res, err := client2.DeleteUser(ctx, req)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, "user deleted", res.GetMessage())
}

func TestGetUsers(t *testing.T) {
	app := setupApp()

	go func() {
		if err := app.srv.StartGrpc(); err != nil {
			log.Fatalf("failed to serve grpc: %v", err)
		}
	}()

	conn, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	defer func() {
		_ = app.db.Stop()
		_ = app.srv.Stop()
		_ = conn.Close()
	}()

	client := snv1.NewUsersClient(conn)
	ctx := context.Background()
	req := &snv1.GetUsersRequest{
		Limit:  4,
		Offset: 0,
	}

	res, err := client.GetUsers(ctx, req)
	assert.NoError(t, err)
	assert.NotNil(t, res)
}

func TestGetUserById(t *testing.T) {
	app := setupApp()

	go func() {
		if err := app.srv.StartGrpc(); err != nil {
			log.Fatalf("failed to serve grpc: %v", err)
		}
	}()

	conn, err := grpc.NewClient(app.srv.GrpcAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	defer func() {
		_ = app.db.Stop()
		_ = app.srv.Stop()
		_ = conn.Close()
	}()

	client := snv1.NewUsersClient(conn)
	ctx := context.Background()
	req := &snv1.GetUserByIdRequest{
		Id: 1,
	}

	res, err := client.GetUserById(ctx, req)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, req.GetId(), res.GetUser().GetId())
}
