package user

import "time"

type User struct {
	Id        int       `db:"id"`
	Name      string    `db:"name"`
	Surname   string    `db:"surname"`
	Email     string    `db:"email"`
	Password  string    `db:"password"`
	IsAdmin   bool      `db:"is_admin"`
	UpdatedAt time.Time `db:"created_at"`
	CreatedAt time.Time `db:"updated_at"`
}
