package user

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"social-network/pkg/logger"
	"strings"

	"github.com/jmoiron/sqlx"
)

type UserRepo struct {
	log  logger.Logger
	conn *sqlx.DB
}

func New(log logger.Logger, conn *sqlx.DB) *UserRepo {
	return &UserRepo{
		log:  log,
		conn: conn,
	}
}

func (r *UserRepo) GetConn() *sqlx.DB {
	return r.conn
}

func (r *UserRepo) CreateUser(ctx context.Context, u *User) error {
	query := "INSERT INTO users(name, surname, email, password, is_admin) VALUES(:name, :surname, :email, :password, :is_admin)"

	if _, err := r.conn.NamedExecContext(ctx, query, &u); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *UserRepo) UpdateUser(ctx context.Context, u *User, setParts []string) error {
	query := fmt.Sprintf("UPDATE users SET %s WHERE id = :id", strings.Join(setParts, ", "))

	if _, err := r.conn.NamedExecContext(ctx, query, &u); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *UserRepo) DeleteUser(ctx context.Context, id int) error {
	query := "DELETE FROM users WHERE id=$1"

	if _, err := r.conn.ExecContext(ctx, query, id); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *UserRepo) GetUsers(ctx context.Context, filter, sort, order string, limit, offset int) ([]User, error) {
	query := fmt.Sprintf("SELECT * FROM users WHERE name LIKE $1 ORDER BY %s %s LIMIT $2 OFFSET $3", sort, order)

	var users []User
	if err := r.conn.SelectContext(ctx, &users, query, filter, limit, offset); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("query failed: %w", err)
	}

	return users, nil
}

func (r *UserRepo) GetUserById(ctx context.Context, id int) (*User, error) {
	query := "SELECT * FROM users WHERE id=$1"

	u := &User{}
	if err := r.conn.GetContext(ctx, u, query, id); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("query failed: %w", err)
	}

	return u, nil
}

func (r *UserRepo) AuthenticateUser(ctx context.Context, email string) (string, error) {
	query := "SELECT password FROM users WHERE email=$1"

	var pass string
	if err := r.conn.GetContext(ctx, &pass, query, email); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", nil
		}
		return "", fmt.Errorf("query failed: %w", err)
	}

	return pass, nil
}
