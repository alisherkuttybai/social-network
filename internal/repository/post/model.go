package post

import "time"

type Post struct {
	Id        int       `db:"id"`
	Title     string    `db:"title"`
	Author    string    `db:"author"`
	Text      string    `db:"text"`
	Likes     int       `db:"likes"`
	IsPublic  bool      `db:"is_public"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}
