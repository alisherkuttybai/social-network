package post

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"social-network/pkg/logger"
	"strings"

	"github.com/jmoiron/sqlx"
)

type PostRepo struct {
	log  logger.Logger
	conn *sqlx.DB
}

func New(log logger.Logger, conn *sqlx.DB) *PostRepo {
	return &PostRepo{
		log:  log,
		conn: conn,
	}
}

func (r *PostRepo) GetConn() *sqlx.DB {
	return r.conn
}

func (r *PostRepo) CreatePost(ctx context.Context, p *Post) error {
	query := "INSERT INTO posts(title, author, text, likes, is_public) VALUES(:title, :author, :text, :likes, :is_public)"

	if _, err := r.conn.NamedExecContext(ctx, query, &p); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *PostRepo) UpdatePost(ctx context.Context, p *Post, setParts []string) error {
	query := fmt.Sprintf("UPDATE posts SET %s WHERE id = :id", strings.Join(setParts, ", "))

	if _, err := r.conn.NamedExecContext(ctx, query, &p); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *PostRepo) DeletePost(ctx context.Context, id int) error {
	query := "DELETE FROM posts WHERE id=$1"

	if _, err := r.conn.ExecContext(ctx, query, id); err != nil {
		return fmt.Errorf("exec failed: %w", err)
	}

	return nil
}

func (r *PostRepo) GetPosts(ctx context.Context, filter, sort, order string, limit, offset int) ([]Post, error) {
	query := fmt.Sprintf("SELECT * FROM posts WHERE title LIKE $1 ORDER BY %s %s LIMIT $2 OFFSET $3", sort, order)

	var posts []Post
	if err := r.conn.SelectContext(ctx, &posts, query, filter, limit, offset); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("query failed: %w", err)
	}

	return posts, nil
}

func (r *PostRepo) GetPostById(ctx context.Context, id int) (*Post, error) {
	query := "SELECT * FROM posts WHERE id=$1"

	u := &Post{}
	if err := r.conn.GetContext(ctx, u, query, id); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("query failed: %w", err)
	}

	return u, nil
}
