package repository

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"social-network/pkg/logger"

	"social-network/internal/repository/post"
	"social-network/internal/repository/user"
)

var (
	ErrNotFound = errors.New("data not found")
)

type Repository struct {
	conn *sqlx.DB
	User *user.UserRepo
	Post *post.PostRepo
}

func New(log logger.Logger, conn *sqlx.DB) *Repository {
	return &Repository{
		conn: conn,
		User: user.New(log, conn),
		Post: post.New(log, conn),
	}
}
