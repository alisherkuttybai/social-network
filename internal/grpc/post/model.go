package post

import (
	"google.golang.org/protobuf/types/known/timestamppb"
	snv1 "social-network/protos/pkg/social_network_v1"
	"time"
)

type Post struct {
	Id        int       `json:"id" db:"id"`
	Title     string    `json:"title" db:"title"`
	Author    string    `json:"author" db:"author"`
	Text      string    `json:"text" db:"text"`
	Likes     int       `json:"likes" db:"likes"`
	IsPublic  bool      `json:"is_public" db:"is_public"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
}

func convertToProtoPost(p *Post) *snv1.Post {
	return &snv1.Post{
		Id: int64(p.Id),
		Info: &snv1.PostInfo{
			Title:    p.Title,
			Author:   p.Author,
			Text:     p.Text,
			Likes:    int64(p.Likes),
			IsPublic: p.IsPublic,
		},
		CreatedAt: timestamppb.New(p.CreatedAt),
		UpdatedAt: timestamppb.New(p.UpdatedAt),
	}
}
