package post

import (
	"context"
	"social-network/internal/repository"
	"social-network/pkg/transport/grpcserver"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"social-network/pkg/logger"

	snv1 "social-network/protos/pkg/social_network_v1"
)

type PostService interface {
	CreatePost(ctx context.Context, p *Post) error
	UpdatePost(ctx context.Context, id int, p *Post) error
	DeletePost(ctx context.Context, id int) error
	GetPosts(ctx context.Context, filter, sort, order string, limit, offset int) ([]*Post, error)
	GetPostById(ctx context.Context, id int) (*Post, error)
}

type serverAPI struct {
	snv1.UnimplementedPostsServer

	log  logger.Logger
	post PostService
}

func New(log logger.Logger, gRPC *grpc.Server, p PostService) {
	snv1.RegisterPostsServer(gRPC, &serverAPI{log: log, post: p})
}

func (s *serverAPI) CreatePost(ctx context.Context, req *snv1.CreatePostRequest) (*snv1.CreatePostResponse, error) {
	if err := grpcserver.ExtractClaims(ctx); err != nil {
		s.log.Error("failed to authorize", logger.Err(err))
		return nil, status.Error(codes.Unauthenticated, "failed to authorize")
	}

	p := &Post{
		Title:    req.GetInfo().GetTitle(),
		Author:   req.GetInfo().GetAuthor(),
		Text:     req.GetInfo().GetText(),
		Likes:    int(req.GetInfo().GetLikes()),
		IsPublic: req.GetInfo().GetIsPublic(),
	}

	if err := s.post.CreatePost(ctx, p); err != nil {
		s.log.Error("failed to create post", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.CreatePostResponse{
		Status:  int64(codes.OK),
		Message: "post created",
	}, nil
}

func (s *serverAPI) UpdatePost(ctx context.Context, req *snv1.UpdatePostRequest) (*snv1.UpdatePostResponse, error) {
	if err := grpcserver.ExtractClaims(ctx); err != nil {
		s.log.Error("failed to authorize", logger.Err(err))
		return nil, status.Error(codes.Unauthenticated, "failed to authorize")
	}

	p := &Post{
		Title:    req.GetInfo().GetTitle(),
		Author:   req.GetInfo().GetAuthor(),
		Text:     req.GetInfo().GetText(),
		Likes:    int(req.GetInfo().GetLikes()),
		IsPublic: req.GetInfo().GetIsPublic(),
	}

	if err := s.post.UpdatePost(ctx, int(req.GetId()), p); err != nil {
		s.log.Error("failed to update post", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.UpdatePostResponse{
		Status:  int64(codes.OK),
		Message: "post updated",
	}, nil
}

func (s *serverAPI) DeletePost(ctx context.Context, req *snv1.DeletePostRequest) (*snv1.DeletePostResponse, error) {
	if err := grpcserver.ExtractClaims(ctx); err != nil {
		s.log.Error("failed to authorize", logger.Err(err))
		return nil, status.Error(codes.Unauthenticated, "failed to authorize")
	}

	if err := s.post.DeletePost(ctx, int(req.GetId())); err != nil {
		s.log.Error("failed to delete post", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.DeletePostResponse{
		Status:  int64(codes.OK),
		Message: "post deleted",
	}, nil
}

func (s *serverAPI) GetPosts(ctx context.Context, req *snv1.GetPostsRequest) (*snv1.GetPostsResponse, error) {
	posts, err := s.post.GetPosts(ctx, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset()))
	if err != nil {
		s.log.Error("failed to get posts", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	if posts == nil {
		s.log.Error("posts not found in database", logger.Err(repository.ErrNotFound))
		return nil, status.Error(codes.NotFound, "post not found")
	}

	var protoPosts []*snv1.Post
	for _, p := range posts {
		protoPost := convertToProtoPost(p)
		protoPosts = append(protoPosts, protoPost)
	}

	return &snv1.GetPostsResponse{
		Posts: protoPosts,
	}, nil
}

func (s *serverAPI) GetPostById(ctx context.Context, req *snv1.GetPostByIdRequest) (*snv1.GetPostByIdResponse, error) {
	p, err := s.post.GetPostById(ctx, int(req.GetId()))
	if err != nil {
		s.log.Error("failed to get post", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	if p == nil {
		s.log.Error("post not found in database", logger.Err(repository.ErrNotFound))
		return nil, status.Error(codes.NotFound, "post not found")
	}

	return &snv1.GetPostByIdResponse{
		Post: convertToProtoPost(p),
	}, nil
}
