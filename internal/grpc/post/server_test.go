package post

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net"
	"social-network/pkg/logger"
	snv1 "social-network/protos/pkg/social_network_v1"
	"testing"
)

func startGRPCServer(t *testing.T, log *logger.MockLogger, postService *MockPostService) (*grpc.ClientConn, func()) {
	s := grpc.NewServer()
	snv1.RegisterPostsServer(s, &serverAPI{log: log, post: postService})

	lis, err := net.Listen("tcp", "localhost:0")
	require.NoError(t, err)

	go func() {
		require.NoError(t, s.Serve(lis))
	}()

	conn, err := grpc.Dial(lis.Addr().String(), grpc.WithInsecure())
	require.NoError(t, err)

	return conn, func() {
		s.Stop()
		conn.Close()
	}
}

func TestGetPosts(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostsRequest{
			Filter: "",
			Sort:   "date",
			Order:  "desc",
			Limit:  10,
			Offset: 0,
		}

		posts := []*Post{
			{Id: 1, Title: "First Post", Text: "This is the content of the first post"},
		}

		postService.On("GetPosts", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(posts, nil)

		res, err := client.GetPosts(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, res)
		require.Len(t, res.Posts, len(posts))

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("InternalError", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostsRequest{
			Filter: "",
			Sort:   "date",
			Order:  "desc",
			Limit:  10,
			Offset: 0,
		}

		postService.On("GetPosts", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(nil, fmt.Errorf("internal error"))
		log.On("Error", "failed to get posts", mock.Anything).Return()

		res, err := client.GetPosts(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
		require.Equal(t, "internal error", st.Message())

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("PostsNotFound", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostsRequest{
			Filter: "",
			Sort:   "date",
			Order:  "desc",
			Limit:  10,
			Offset: 0,
		}

		postService.On("GetPosts", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(nil, nil)
		log.On("Error", "posts not found in database", mock.Anything).Return()

		res, err := client.GetPosts(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.NotFound, st.Code())
		require.Equal(t, "post not found", st.Message())

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})
}

func TestGetPostById(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostByIdRequest{Id: 1}

		post := &Post{
			Id:    1,
			Title: "First Post",
			Text:  "This is the content of the first post",
		}

		postService.On("GetPostById", mock.Anything, int(req.GetId())).Return(post, nil)

		res, err := client.GetPostById(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, res)
		require.Equal(t, post.Id, int(res.GetPost().GetId()))
		require.Equal(t, post.Title, res.GetPost().GetInfo().GetTitle())
		require.Equal(t, post.Text, res.GetPost().GetInfo().GetText())

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("InternalError", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostByIdRequest{Id: 1}

		postService.On("GetPostById", mock.Anything, int(req.GetId())).Return(nil, fmt.Errorf("internal error"))
		log.On("Error", "failed to get post", mock.Anything).Return()

		res, err := client.GetPostById(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
		require.Equal(t, "internal error", st.Message())

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("PostNotFound", func(t *testing.T) {
		log := new(logger.MockLogger)
		postService := new(MockPostService)
		conn, cleanup := startGRPCServer(t, log, postService)
		defer cleanup()

		client := snv1.NewPostsClient(conn)

		ctx := context.Background()
		req := &snv1.GetPostByIdRequest{Id: 1}

		postService.On("GetPostById", mock.Anything, int(req.GetId())).Return(nil, nil)
		log.On("Error", "post not found in database", mock.Anything).Return()

		res, err := client.GetPostById(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.NotFound, st.Code())
		require.Equal(t, "post not found", st.Message())

		postService.AssertExpectations(t)
		log.AssertExpectations(t)
	})
}
