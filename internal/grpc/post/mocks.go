package post

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type MockPostService struct {
	mock.Mock
}

func (m *MockPostService) CreatePost(ctx context.Context, p *Post) error {
	args := m.Called(ctx, p)
	return args.Error(0)
}

func (m *MockPostService) UpdatePost(ctx context.Context, id int, p *Post) error {
	args := m.Called(ctx, id, p)
	return args.Error(0)
}

func (m *MockPostService) DeletePost(ctx context.Context, id int) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func (m *MockPostService) GetPosts(ctx context.Context, filter, sort, order string, limit, offset int) ([]*Post, error) {
	args := m.Called(ctx, filter, sort, order, limit, offset)
	if posts, ok := args.Get(0).([]*Post); ok {
		return posts, args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockPostService) GetPostById(ctx context.Context, id int) (*Post, error) {
	args := m.Called(ctx, id)
	user, _ := args.Get(0).(*Post)
	return user, args.Error(1)
}
