package user

import (
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
	snv1 "social-network/protos/pkg/social_network_v1"
)

type User struct {
	Id        int       `json:"id"`
	Name      string    `json:"name"`
	Surname   string    `json:"surname"`
	Email     string    `json:"email"`
	Password  string    `json:"password"`
	IsAdmin   bool      `json:"is_admin"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func convertToProtoUser(u *User) *snv1.User {
	return &snv1.User{
		Id: int64(u.Id),
		Info: &snv1.UserInfo{
			Name:     u.Name,
			Surname:  u.Surname,
			Email:    u.Email,
			Password: u.Password,
			IsAdmin:  u.IsAdmin,
		},
		CreatedAt: timestamppb.New(u.CreatedAt),
		UpdatedAt: timestamppb.New(u.UpdatedAt),
	}
}
