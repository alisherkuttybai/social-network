package user

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"social-network/pkg/transport/grpcserver"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"social-network/internal/repository"
	"social-network/pkg/logger"

	snv1 "social-network/protos/pkg/social_network_v1"
)

type UserService interface {
	CreateUser(ctx context.Context, u *User) error
	UpdateUser(ctx context.Context, id int, u *User) error
	DeleteUser(ctx context.Context, id int) error
	GetUsers(ctx context.Context, filter, sort, order string, limit, offset int) ([]*User, error)
	GetUserById(ctx context.Context, id int) (*User, error)
	AuthenticateUser(ctx context.Context, password, email string) (string, error)
}

type serverAPI struct {
	snv1.UnimplementedUsersServer

	log  logger.Logger
	user UserService
}

func New(log logger.Logger, gRPC *grpc.Server, u UserService) {
	snv1.RegisterUsersServer(gRPC, &serverAPI{log: log, user: u})
}

func (s *serverAPI) CreateUser(ctx context.Context, req *snv1.CreateUserRequest) (*snv1.CreateUserResponse, error) {
	u := &User{
		Name:     req.GetInfo().GetName(),
		Surname:  req.GetInfo().GetSurname(),
		Email:    req.GetInfo().GetEmail(),
		Password: req.GetInfo().GetPassword(),
		IsAdmin:  req.GetInfo().GetIsAdmin(),
	}

	if err := s.user.CreateUser(ctx, u); err != nil {
		s.log.Error("failed to create user", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.CreateUserResponse{
		Status:  int64(codes.OK),
		Message: "user created",
	}, nil
}

func (s *serverAPI) UpdateUser(ctx context.Context, req *snv1.UpdateUserRequest) (*snv1.UpdateUserResponse, error) {
	if err := grpcserver.ExtractClaims(ctx); err != nil {
		s.log.Error("failed to authorize", logger.Err(err))
		return nil, status.Error(codes.Unauthenticated, "failed to authorize")
	}

	u := &User{
		Name:     req.GetInfo().GetName(),
		Surname:  req.GetInfo().GetSurname(),
		Email:    req.GetInfo().GetEmail(),
		Password: req.GetInfo().GetPassword(),
		IsAdmin:  req.GetInfo().GetIsAdmin(),
	}

	if err := s.user.UpdateUser(ctx, int(req.GetId()), u); err != nil {
		s.log.Error("failed to update user", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.UpdateUserResponse{
		Status:  int64(codes.OK),
		Message: "user updated",
	}, nil
}

func (s *serverAPI) DeleteUser(ctx context.Context, req *snv1.DeleteUserRequest) (*snv1.DeleteUserResponse, error) {
	if err := grpcserver.ExtractClaims(ctx); err != nil {
		s.log.Error("failed to authorize", logger.Err(err))
		return nil, status.Error(codes.Unauthenticated, "failed to authorize")
	}

	if err := s.user.DeleteUser(ctx, int(req.GetId())); err != nil {
		s.log.Error("failed to delete user", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.DeleteUserResponse{
		Status:  int64(codes.OK),
		Message: "user deleted",
	}, nil
}

func (s *serverAPI) GetUsers(ctx context.Context, req *snv1.GetUsersRequest) (*snv1.GetUsersResponse, error) {
	users, err := s.user.GetUsers(ctx, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset()))
	if err != nil {
		s.log.Error("failed to get users", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	if users == nil {
		s.log.Error("users not found in database", logger.Err(repository.ErrNotFound))
		return nil, status.Error(codes.NotFound, "users not found")
	}

	var protoUsers []*snv1.User
	for _, user := range users {
		protoUser := convertToProtoUser(user)
		protoUsers = append(protoUsers, protoUser)
	}

	return &snv1.GetUsersResponse{
		Users: protoUsers,
	}, nil
}

func (s *serverAPI) GetUserById(ctx context.Context, req *snv1.GetUserByIdRequest) (*snv1.GetUserByIdResponse, error) {
	u, err := s.user.GetUserById(ctx, int(req.GetId()))
	if err != nil {
		s.log.Error("failed to get user", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	if u == nil {
		s.log.Error("user not found in database", logger.Err(repository.ErrNotFound))
		return nil, status.Error(codes.NotFound, "user not found")
	}

	return &snv1.GetUserByIdResponse{
		User: convertToProtoUser(u),
	}, nil
}

func (s *serverAPI) AuthenticateUser(ctx context.Context, req *snv1.AuthenticateUserRequest) (*snv1.AuthenticateUserResponse, error) {
	hashPass, err := s.user.AuthenticateUser(ctx, req.GetLogin().GetEmail(), req.GetLogin().GetPassword())
	if err != nil {
		s.log.Error("failed to authenticate", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	if hashPass == "" {
		s.log.Error("failed to authenticate")
		return nil, status.Error(codes.Unauthenticated, "invalid credentials")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": req.GetLogin().GetEmail(),
		"exp":   time.Now().Add(time.Minute * 15).Unix(),
	})

	tokenString, err := token.SignedString(grpcserver.JwtKey)
	if err != nil {
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &snv1.AuthenticateUserResponse{
		Token: tokenString,
	}, nil
}
