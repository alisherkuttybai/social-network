package user

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type MockUserService struct {
	mock.Mock
}

func (m *MockUserService) CreateUser(ctx context.Context, user *User) error {
	args := m.Called(ctx, user)
	return args.Error(0)
}

func (m *MockUserService) UpdateUser(ctx context.Context, id int, u *User) error {
	args := m.Called(ctx, id, u)
	return args.Error(0)
}

func (m *MockUserService) DeleteUser(ctx context.Context, id int) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func (m *MockUserService) GetUsers(ctx context.Context, filter, sort, order string, limit, offset int) ([]*User, error) {
	args := m.Called(ctx, filter, sort, order, limit, offset)
	if users, ok := args.Get(0).([]*User); ok {
		return users, args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockUserService) GetUserById(ctx context.Context, id int) (*User, error) {
	args := m.Called(ctx, id)
	user, _ := args.Get(0).(*User)
	return user, args.Error(1)
}

func (m *MockUserService) AuthenticateUser(ctx context.Context, password, email string) (string, error) {
	args := m.Called(ctx, password, email)
	return args.String(0), args.Error(1)
}
