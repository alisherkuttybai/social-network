package user

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net"
	"social-network/pkg/logger"
	snv1 "social-network/protos/pkg/social_network_v1"
	"testing"
)

func startGRPCServer(t *testing.T, log *logger.MockLogger, userService *MockUserService) (*grpc.ClientConn, func()) {
	s := grpc.NewServer()
	snv1.RegisterUsersServer(s, &serverAPI{log: log, user: userService})

	lis, err := net.Listen("tcp", "localhost:0")
	require.NoError(t, err)

	go func() {
		require.NoError(t, s.Serve(lis))
	}()

	conn, err := grpc.Dial(lis.Addr().String(), grpc.WithInsecure())
	require.NoError(t, err)

	return conn, func() {
		s.Stop()
		conn.Close()
	}
}

func TestCreateUser(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.CreateUserRequest{
			Info: &snv1.UserInfo{
				Name:     "John",
				Surname:  "Doe",
				Email:    "john.doe@example.com",
				Password: "password123",
				IsAdmin:  false,
			},
		}

		u := &User{
			Name:     req.GetInfo().GetName(),
			Surname:  req.GetInfo().GetSurname(),
			Email:    req.GetInfo().GetEmail(),
			Password: req.GetInfo().GetPassword(),
			IsAdmin:  req.GetInfo().GetIsAdmin(),
		}

		userService.On("CreateUser", mock.Anything, u).Return(nil)

		res, err := client.CreateUser(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, res)
		require.Equal(t, int64(codes.OK), res.Status)
		require.Equal(t, "user created", res.Message)

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("InternalError", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.CreateUserRequest{
			Info: &snv1.UserInfo{
				Name:     "John",
				Surname:  "Doe",
				Email:    "john.doe@example.com",
				Password: "password123",
				IsAdmin:  false,
			},
		}

		u := &User{
			Name:     req.GetInfo().GetName(),
			Surname:  req.GetInfo().GetSurname(),
			Email:    req.GetInfo().GetEmail(),
			Password: req.GetInfo().GetPassword(),
			IsAdmin:  req.GetInfo().GetIsAdmin(),
		}

		userService.On("CreateUser", mock.Anything, u).Return(fmt.Errorf("internal error"))
		log.On("Error", "failed to create user", mock.Anything).Return()

		res, err := client.CreateUser(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
		require.Equal(t, "internal error", st.Message())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})
}

func TestGetUsers(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUsersRequest{
			Filter: "",
			Sort:   "name",
			Order:  "asc",
			Limit:  10,
			Offset: 0,
		}

		mockUsers := []*User{
			{Name: "John", Surname: "Doe", Email: "john.doe@example.com", Password: "password123", IsAdmin: false},
			{Name: "Jane", Surname: "Smith", Email: "jane.smith@example.com", Password: "password456", IsAdmin: false},
		}

		userService.On("GetUsers", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(mockUsers, nil)

		res, err := client.GetUsers(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, res)
		require.Len(t, res.Users, len(mockUsers))

		for i, protoUser := range res.Users {
			require.Equal(t, mockUsers[i].Name, protoUser.GetInfo().GetName())
			require.Equal(t, mockUsers[i].Surname, protoUser.GetInfo().GetSurname())
			require.Equal(t, mockUsers[i].Email, protoUser.GetInfo().GetEmail())
			require.Equal(t, mockUsers[i].Password, protoUser.GetInfo().GetPassword())
			require.Equal(t, mockUsers[i].IsAdmin, protoUser.GetInfo().GetIsAdmin())
		}

		userService.AssertExpectations(t)
	})

	t.Run("InternalError", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUsersRequest{
			Filter: "",
			Sort:   "name",
			Order:  "asc",
			Limit:  10,
			Offset: 0,
		}

		userService.On("GetUsers", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(nil, status.Error(codes.Internal, "internal error"))
		log.On("Error", "failed to get users", mock.Anything).Return()

		res, err := client.GetUsers(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
		require.Equal(t, "internal error", st.Message())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("UsersNotFound", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUsersRequest{
			Filter: "",
			Sort:   "name",
			Order:  "asc",
			Limit:  10,
			Offset: 0,
		}

		userService.On("GetUsers", mock.Anything, req.GetFilter(), req.GetSort(), req.GetOrder(), int(req.GetLimit()), int(req.GetOffset())).Return(nil, nil)
		log.On("Error", "users not found in database", mock.Anything).Return()

		res, err := client.GetUsers(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.NotFound, st.Code())
		require.Equal(t, "users not found", st.Message())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})
}

func TestGetUserById(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUserByIdRequest{Id: 1}
		expectedUser := &User{
			Id:      1,
			Name:    "John",
			Surname: "Doe",
			Email:   "john.doe@example.com",
		}

		userService.On("GetUserById", mock.Anything, int(req.GetId())).Return(expectedUser, nil)

		res, err := client.GetUserById(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, res)
		require.Equal(t, expectedUser.Name, res.GetUser().GetInfo().GetName())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("InternalError", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUserByIdRequest{Id: 1}

		userService.On("GetUserById", mock.Anything, int(req.GetId())).Return(nil, status.Error(codes.Internal, "internal error"))
		log.On("Error", "failed to get user", mock.Anything).Return()

		res, err := client.GetUserById(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
		require.Equal(t, "internal error", st.Message())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})

	t.Run("UserNotFound", func(t *testing.T) {
		log := new(logger.MockLogger)
		userService := new(MockUserService)
		conn, cleanup := startGRPCServer(t, log, userService)
		defer cleanup()

		client := snv1.NewUsersClient(conn)

		ctx := context.Background()
		req := &snv1.GetUserByIdRequest{Id: 1}

		userService.On("GetUserById", mock.Anything, int(req.GetId())).Return(nil, nil)
		log.On("Error", "user not found in database", mock.Anything).Return()

		res, err := client.GetUserById(ctx, req)
		require.Error(t, err)
		require.Nil(t, res)
		st, _ := status.FromError(err)
		require.Equal(t, codes.NotFound, st.Code())
		require.Equal(t, "user not found", st.Message())

		userService.AssertExpectations(t)
		log.AssertExpectations(t)
	})
}
