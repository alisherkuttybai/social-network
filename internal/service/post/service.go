package post

import (
	"context"
	"fmt"
	"social-network/pkg/logger"
	"time"

	"github.com/jmoiron/sqlx"
	handlerModel "social-network/internal/grpc/post"
	repoModel "social-network/internal/repository/post"
)

type PostRepo interface {
	GetConn() *sqlx.DB
	CreatePost(ctx context.Context, p *repoModel.Post) error
	UpdatePost(ctx context.Context, p *repoModel.Post, setParts []string) error
	DeletePost(ctx context.Context, id int) error
	GetPosts(ctx context.Context, filter, sort, order string, limit, offset int) ([]repoModel.Post, error)
	GetPostById(ctx context.Context, id int) (*repoModel.Post, error)
}

type PostService struct {
	log  logger.Logger
	repo PostRepo
}

func New(log logger.Logger, repo PostRepo) *PostService {
	return &PostService{
		log:  log,
		repo: repo,
	}
}

func (s *PostService) CreatePost(ctx context.Context, p *handlerModel.Post) error {
	dbPost := &repoModel.Post{
		Title:    p.Title,
		Author:   p.Author,
		Text:     p.Text,
		Likes:    p.Likes,
		IsPublic: p.IsPublic,
	}

	if err := s.repo.CreatePost(ctx, dbPost); err != nil {
		return fmt.Errorf("create post: %w", err)
	}
	return nil
}

func (s *PostService) UpdatePost(ctx context.Context, id int, p *handlerModel.Post) error {
	now := time.Now()

	dbPost := &repoModel.Post{
		Id:        id,
		Title:     p.Title,
		Author:    p.Author,
		Text:      p.Text,
		Likes:     p.Likes,
		IsPublic:  p.IsPublic,
		UpdatedAt: now,
	}

	var setParts []string
	if dbPost.Title != "" {
		setParts = append(setParts, "title = :title")
	}
	if dbPost.Author != "" {
		setParts = append(setParts, "author = :author")
	}
	if dbPost.Text != "" {
		setParts = append(setParts, "text = :text")
	}
	if dbPost.Likes != 0 {
		setParts = append(setParts, "likes = :likes")
	}
	if dbPost.IsPublic {
		setParts = append(setParts, "is_public = :is_public")
	}

	if len(setParts) == 0 {
		return fmt.Errorf("no fields to update")
	}

	if err := s.repo.UpdatePost(ctx, dbPost, setParts); err != nil {
		return fmt.Errorf("get posts: %w", err)
	}
	return nil
}

func (s *PostService) DeletePost(ctx context.Context, id int) error {
	if err := s.repo.DeletePost(ctx, id); err != nil {
		return fmt.Errorf("delete repoModel: %w", err)
	}
	return nil
}

func (s *PostService) GetPosts(ctx context.Context, filter, sort, order string, limit, offset int) ([]*handlerModel.Post, error) {
	filter = "%" + filter + "%"

	if sort == "" {
		sort = "id" // Default sort column
	}
	if order != "asc" && order != "desc" {
		order = "asc" // Default sort order
	}
	if limit <= 0 {
		limit = 10 // Default limit
	}
	if offset < 0 {
		offset = 0 // Default offset
	}

	posts, err := s.repo.GetPosts(ctx, filter, sort, order, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("get posts: %w", err)
	}

	if posts == nil {
		return nil, nil
	}

	var handlerUsers []*handlerModel.Post
	for _, p := range posts {
		handlerUser := &handlerModel.Post{
			Id:        p.Id,
			Title:     p.Title,
			Author:    p.Author,
			Text:      p.Text,
			Likes:     p.Likes,
			IsPublic:  p.IsPublic,
			CreatedAt: p.CreatedAt,
			UpdatedAt: p.UpdatedAt,
		}
		handlerUsers = append(handlerUsers, handlerUser)
	}

	return handlerUsers, nil
}

func (s *PostService) GetPostById(ctx context.Context, id int) (*handlerModel.Post, error) {
	p, err := s.repo.GetPostById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("get post: %w", err)
	}

	if p == nil {
		return nil, nil
	}

	return &handlerModel.Post{
		Id:        p.Id,
		Title:     p.Title,
		Author:    p.Author,
		Text:      p.Text,
		Likes:     p.Likes,
		IsPublic:  p.IsPublic,
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
	}, nil
}
