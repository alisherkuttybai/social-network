package service

import (
	"social-network/internal/repository"
	"social-network/internal/service/post"
	"social-network/internal/service/user"
	"social-network/pkg/logger"
)

type Service struct {
	User *user.UserService
	Post *post.PostService
}

func New(log logger.Logger, repo *repository.Repository) *Service {
	return &Service{
		User: user.New(log, repo.User),
		Post: post.New(log, repo.Post),
	}
}
