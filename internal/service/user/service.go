package user

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
	handlerModel "social-network/internal/grpc/user"
	"time"

	repoModel "social-network/internal/repository/user"
	"social-network/pkg/logger"
)

type UserRepo interface {
	GetConn() *sqlx.DB
	CreateUser(ctx context.Context, u *repoModel.User) error
	UpdateUser(ctx context.Context, u *repoModel.User, setParts []string) error
	DeleteUser(ctx context.Context, id int) error
	GetUsers(ctx context.Context, filter, sort, order string, limit, offset int) ([]repoModel.User, error)
	GetUserById(ctx context.Context, id int) (*repoModel.User, error)
	AuthenticateUser(ctx context.Context, email string) (string, error)
}

type UserService struct {
	log  logger.Logger
	repo UserRepo
}

func New(log logger.Logger, repo UserRepo) *UserService {
	return &UserService{
		log:  log,
		repo: repo,
	}
}

func (s *UserService) CreateUser(ctx context.Context, u *handlerModel.User) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("failed to hash password: %w", err)
	}

	dbUser := &repoModel.User{
		Name:     u.Name,
		Surname:  u.Surname,
		Email:    u.Email,
		Password: string(bytes),
		IsAdmin:  u.IsAdmin,
	}

	if err := s.repo.CreateUser(ctx, dbUser); err != nil {
		return fmt.Errorf("create user: %w", err)
	}
	return nil
}

func (s *UserService) UpdateUser(ctx context.Context, id int, u *handlerModel.User) error {
	now := time.Now()

	dbUser := &repoModel.User{
		Id:        id,
		Name:      u.Name,
		Surname:   u.Surname,
		Email:     u.Email,
		Password:  u.Password,
		IsAdmin:   u.IsAdmin,
		UpdatedAt: now,
	}

	var setParts []string
	if dbUser.Name != "" {
		setParts = append(setParts, "name = :name")
	}
	if dbUser.Surname != "" {
		setParts = append(setParts, "surname = :surname")
	}
	if dbUser.Email != "" {
		setParts = append(setParts, "email = :email")
	}
	if dbUser.Password != "" {
		bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return fmt.Errorf("failed to hash password: %w", err)
		}
		dbUser.Password = string(bytes)
		setParts = append(setParts, "password = :password")
	}
	if dbUser.IsAdmin {
		setParts = append(setParts, "is_admin = :is_admin")
	}

	if len(setParts) == 0 {
		return fmt.Errorf("no fields to update")
	}

	if err := s.repo.UpdateUser(ctx, dbUser, setParts); err != nil {
		return fmt.Errorf("get users: %w", err)
	}
	return nil
}

func (s *UserService) DeleteUser(ctx context.Context, id int) error {
	if err := s.repo.DeleteUser(ctx, id); err != nil {
		return fmt.Errorf("delete user: %w", err)
	}
	return nil
}

func (s *UserService) GetUsers(ctx context.Context, filter, sort, order string, limit, offset int) ([]*handlerModel.User, error) {
	filter = "%" + filter + "%"

	if sort == "" {
		sort = "id" // Default sort column
	}
	if order != "asc" && order != "desc" {
		order = "asc" // Default sort order
	}
	if limit <= 0 {
		limit = 10 // Default limit
	}
	if offset < 0 {
		offset = 0 // Default offset
	}

	users, err := s.repo.GetUsers(ctx, filter, sort, order, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("get users: %w", err)
	}

	if users == nil {
		return nil, nil
	}

	var handlerUsers []*handlerModel.User
	for _, u := range users {
		handlerUser := &handlerModel.User{
			Id:        u.Id,
			Name:      u.Name,
			Surname:   u.Surname,
			Email:     u.Email,
			Password:  u.Password,
			IsAdmin:   u.IsAdmin,
			CreatedAt: u.CreatedAt,
			UpdatedAt: u.UpdatedAt,
		}
		handlerUsers = append(handlerUsers, handlerUser)
	}

	return handlerUsers, nil
}

func (s *UserService) GetUserById(ctx context.Context, id int) (*handlerModel.User, error) {
	u, err := s.repo.GetUserById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("get user: %w", err)
	}

	if u == nil {
		return nil, nil
	}

	return &handlerModel.User{
		Id:        u.Id,
		Name:      u.Name,
		Surname:   u.Surname,
		Email:     u.Email,
		Password:  u.Password,
		IsAdmin:   u.IsAdmin,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}, nil
}

func (s *UserService) AuthenticateUser(ctx context.Context, email, password string) (string, error) {
	hashPass, err := s.repo.AuthenticateUser(ctx, email)
	if err != nil {
		return "", fmt.Errorf("failed authenticate user: %w", err)
	}

	if hashPass == "" {
		return "", nil
	}

	if err := bcrypt.CompareHashAndPassword([]byte(hashPass), []byte(password)); err != nil {
		s.log.Error("failed compare password", logger.Err(err))
		return "", nil
	}

	return hashPass, nil
}
