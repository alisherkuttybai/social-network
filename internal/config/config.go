package config

import (
	"fmt"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	Config struct {
		App       App       `yaml:"app"`
		Transport Transport `yaml:"transport"`
		DB        DB        `yaml:"db"`
	}

	App struct {
		LogLevel string `yaml:"log-level"  env-required:"true"`
		Name     string `yaml:"name" env-required:"true"`
		Version  string `yaml:"version" env-required:"true"`
	}

	Transport struct {
		HTTP    Http    `yaml:"http"`
		GRPC    Grpc    `yaml:"grpc"`
		Swagger Swagger `yaml:"swagger"`
	}

	Http struct {
		Host        string        `yaml:"host" env-required:"true"`
		Port        string        `yaml:"port" env-required:"true"`
		Timeout     time.Duration `yaml:"timeout"`
		IdleTimeout time.Duration `yaml:"idle-timeout"`
	}

	Grpc struct {
		Host    string        `yaml:"host" env-required:"true"`
		Port    string        `yaml:"port" env-required:"true"`
		Timeout time.Duration `yaml:"timeout" env-required:"true"`
	}

	Swagger struct {
		Host string `yaml:"host" env-required:"true"`
		Port string `yaml:"port" env-required:"true"`
	}

	DB struct {
		Postgres Postgres `yaml:"postgres"`
	}

	Postgres struct {
		Host string `yaml:"host" env-required:"true"`
		Port string `yaml:"port" env-required:"true"`
		User string `yaml:"user" env-required:"true"`
		Pass string `yaml:"pass" env-required:"true"`
		Name string `yaml:"name" env-required:"true"`
	}
)

func New(configPath string) (*Config, error) {
	cfg := &Config{}
	if err := cleanenv.ReadConfig(configPath, cfg); err != nil {
		return nil, fmt.Errorf("failed read config: %w", err)
	}
	return cfg, nil
}
