package grpcserver

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi/v5"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	httpSwagger "github.com/swaggo/http-swagger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"net"
	"net/http"
	"strings"

	"social-network/internal/config"
)

var JwtKey = []byte("test_token231654")

type Server struct {
	Grpc           *grpc.Server
	http           *http.Server
	Mux            *runtime.ServeMux
	HttpOpts       []grpc.DialOption
	GrpcAddress    string
	HttpAddress    string
	swaggerAddress string
}

func New(cfg *config.Transport) (*Server, error) {
	gRpcSrv := grpc.NewServer()
	reflection.Register(gRpcSrv)

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	grpcAddress := fmt.Sprintf("%s:%s", cfg.GRPC.Host, cfg.GRPC.Port)
	httpAddress := fmt.Sprintf("%s:%s", cfg.HTTP.Host, cfg.HTTP.Port)
	swaggerAddress := fmt.Sprintf("%s:%s", cfg.Swagger.Host, cfg.Swagger.Port)

	httpSrv := &http.Server{
		Addr:    httpAddress,
		Handler: allowCORS(mux),
	}

	return &Server{
		Grpc:           gRpcSrv,
		http:           httpSrv,
		Mux:            mux,
		HttpOpts:       opts,
		GrpcAddress:    grpcAddress,
		HttpAddress:    httpAddress,
		swaggerAddress: swaggerAddress,
	}, nil
}

func (srv *Server) StartGrpc() error {
	l, err := net.Listen("tcp", srv.GrpcAddress)
	if err != nil {
		return err
	}
	return srv.Grpc.Serve(l)
}

func (srv *Server) StartHttp() error {
	return srv.http.ListenAndServe()
}

func (srv *Server) Stop() error {
	srv.Grpc.GracefulStop()
	return srv.http.Close()
}

func allowCORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		if r.Method == "OPTIONS" {
			return
		}
		h.ServeHTTP(w, r)
	})
}

func ExtractClaims(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return fmt.Errorf("missing metadata")
	}

	authHeader := md["authorization"]
	if len(authHeader) == 0 {
		return fmt.Errorf("missing authorization header")
	}

	tokenString := strings.TrimPrefix(authHeader[0], "Bearer ")
	if tokenString == authHeader[0] {
		return fmt.Errorf("invalid token format")
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method")
		}
		return JwtKey, nil
	})

	if err != nil {
		return fmt.Errorf("invalid token")
	}

	if token.Valid {
		return nil
	}

	return fmt.Errorf("invalid token claims")
}

func (srv *Server) StartSwaggerSrv() error {
	r := chi.NewRouter()
	r.Get("/swagger/doc.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./protos/api/social_network_v1/social_network.swagger.json")
	})
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:1323/swagger/doc.json"),
	))
	return http.ListenAndServe(srv.swaggerAddress, r)
}
