package logger

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type MockLogger struct {
	mock.Mock
}

func (m *MockLogger) Debug(msg string, args ...any) {
	m.Called(msg, args)
}

func (m *MockLogger) DebugContext(ctx context.Context, msg string, args ...any) {
	m.Called(ctx, msg, args)
}

func (m *MockLogger) Info(msg string, args ...any) {
	m.Called(msg, args)
}

func (m *MockLogger) InfoContext(ctx context.Context, msg string, args ...any) {
	m.Called(ctx, msg, args)
}

func (m *MockLogger) Warn(msg string, args ...any) {
	m.Called(msg, args)
}

func (m *MockLogger) WarnContext(ctx context.Context, msg string, args ...any) {
	m.Called(ctx, msg, args)
}

func (m *MockLogger) Error(msg string, args ...any) {
	m.Called(msg, args)
}

func (m *MockLogger) ErrorContext(ctx context.Context, msg string, args ...any) {
	m.Called(ctx, msg, args)
}
