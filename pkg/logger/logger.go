package logger

import (
	"context"
	"errors"
	"log/slog"
	"os"
	"strings"

	"social-network/pkg/logger/slogpretty"
)

const (
	logLocal = "local"
	logDev   = "dev"
	logProd  = "prod"
)

var errNotValidType = errors.New("not valid logger type")

type Logger interface {
	Debug(msg string, args ...any)
	DebugContext(ctx context.Context, msg string, args ...any)
	Info(msg string, args ...any)
	InfoContext(ctx context.Context, msg string, args ...any)
	Warn(msg string, args ...any)
	WarnContext(ctx context.Context, msg string, args ...any)
	Error(msg string, args ...any)
	ErrorContext(ctx context.Context, msg string, args ...any)
}

func New(logLvl string) (Logger, error) {
	var (
		logger   *slog.Logger
		logLevel = strings.ToLower(logLvl)
	)
	switch logLevel {
	case logLocal:
		logger = setupPrettySlog()
	case logDev:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
				AddSource: true,
				Level:     slog.LevelDebug,
			}),
		)
	case logProd:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
				AddSource: true,
				Level:     slog.LevelInfo,
			}),
		)
	default:
		return nil, errNotValidType
	}

	return logger, nil
}

func setupPrettySlog() *slog.Logger {
	options := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
		},
	}
	handler := options.NewPrettyHandler(os.Stdout)
	return slog.New(handler)
}

func Err(err error) slog.Attr {
	return slog.Attr{
		Key:   "error",
		Value: slog.StringValue(err.Error()),
	}
}
