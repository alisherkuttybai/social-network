package postgres

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/file"
	"time"

	"social-network/internal/config"

	_ "github.com/jackc/pgx/v5"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
)

type Postgres struct {
	Conn   *sqlx.DB
	url    string
	dbname string
}

func New(cfg *config.Postgres) (*Postgres, error) {
	url := postgresURL(cfg)
	db, err := sqlx.Open("pgx", url)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	return &Postgres{
		Conn:   db,
		url:    url,
		dbname: cfg.Name,
	}, nil
}

func (db *Postgres) Migrate() error {
	instance, err := postgres.WithInstance(db.Conn.DB, &postgres.Config{})
	if err != nil {
		return fmt.Errorf("failed create database instance for migrate, err: %w", err)
	}

	files, err := (&file.File{}).Open("./migrations")
	if err != nil {
		return fmt.Errorf("failed open migrations files, err: %w", err)
	}

	m, err := migrate.NewWithInstance("file", files, db.dbname, instance)
	if err != nil {
		return fmt.Errorf("failed create migrate instance, err: %w", err)
	}

	if err = m.Up(); err != nil && !errors.Is(err, errors.New("no change")) {
		return fmt.Errorf("failed migrate up, err: %w", err)
	}

	return nil
}

func (db *Postgres) Stop() error {
	return db.Conn.Close()
}

func postgresURL(cfg *config.Postgres) string {
	return fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		cfg.User, cfg.Pass, cfg.Host, cfg.Port, cfg.Name)
}
